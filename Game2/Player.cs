﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game2
{
    class Player : AnimatedSprite
    {
        private int JoueurVie;
        private string JoueurNom;
        private bool Sauter;
        private Rectangle boudingPlayer;
        public Rectangle playerBoudingBox
        {
            get { return boudingPlayer = new Rectangle((int)positionX, (int)positionY, sTexture.Width/4, sTexture.Height); }
            set { boudingPlayer = value; }
        }
        ////////////////////////////////////////////////////////////////////getter Setter

        ////////////////////////////////////////////////////////////////////Constructeur
        public Player(string Nom, Vector2 positionJoueur): base(positionJoueur)
        {
            this.FramesPerSecond = 30;
            this.JoueurVie = 3;
            this.JoueurNom = Nom;
            //position = new Vector2(50, 50);
            Sauter = true;
           
                //playerBoudingBox = new Rectangle((int)positionX, (int)positionY, 16,32);
  
        }
        public override int OnCollide(AnimatedSprite sprite)
        {
            int touché = 0;
            #region
            if (speed.X > 0)
            {
                if (playerBoudingBox.Bottom > sprite.boundingBox.Top &&
                    playerBoudingBox.Top < sprite.boundingBox.Bottom &&
                    playerBoudingBox.Left < sprite.boundingBox.Left &&
                    playerBoudingBox.Right >= sprite.boundingBox.Left)
                {
                    //this.position = new Vector2(sprite.position.X-this.texture.Width/4, this.position.Y);
                    touché = 1;
                }
            }

            if (speed.X < 0)
            {
                if (this.playerBoudingBox.Bottom > sprite.boundingBox.Top &&
                    this.playerBoudingBox.Top < sprite.boundingBox.Bottom &&
                    this.playerBoudingBox.Right > sprite.boundingBox.Right &&
                    this.playerBoudingBox.Left <= sprite.boundingBox.Right)
                {
                    // this.position = new Vector2(sprite.position.X + sprite.boundingBox.Width, this.position.Y);
                    touché = 1;
                }
            }

            if (speed.Y > 0)
            {
                if (this.playerBoudingBox.Right > sprite.boundingBox.Left &&
                   this.playerBoudingBox.Left < sprite.boundingBox.Right &&
                   playerBoudingBox.Top < sprite.boundingBox.Top &&
                   this.playerBoudingBox.Bottom >= sprite.boundingBox.Top)
                {
                    //_onGround = true;
                    //this.position = new Vector2(this.position.X, sprite.position.Y- this.texture.Height-1);
                    // this.position = new Vector2(this.position.X, sprite.position.Y - this.texture.Height - 1);
                    touché = 1;

                }
            }

            if (speed.Y < 0)
            {
                if (playerBoudingBox.Right > sprite.boundingBox.Left &&
                   playerBoudingBox.Left < sprite.boundingBox.Right &&
                   playerBoudingBox.Bottom > sprite.boundingBox.Bottom &&
                   playerBoudingBox.Top <= sprite.boundingBox.Bottom)
                {
                    //this.position = new Vector2(this.position.X, sprite.position.Y + sprite.boundingBox.Height);
                    touché = 1;

                }
            }
            return touché;
            #endregion
        }

        ////////////////////////////////////////////////////////////////////Methods de La class
        private void Mouvement(GameTime gameTime)
        {


            positionX += (int)speed.X * (float)gameTime.ElapsedGameTime.TotalSeconds;
            positionY += speed.Y;
            //saut
            if (Keyboard.GetState().IsKeyDown(Keys.Space) && Sauter == false)
            {
                positionY -= 5f;
                speed = new Vector2(200,-5f);
                this.Sauter = true;
            }
            if (Sauter == true)
            {
                float i = 1;
                speed += new Vector2(0,0.25f * i);

                Vector2 blocage= new Vector2(0,0.32f * i);
                if(speed.Y>=blocage.Y)
                {
                    speed += blocage;
                }
            }
            if (positionY + sTexture.Height >=800+32)
            {
                Sauter = false;
               positionY= 800-8;
            }
            if (Sauter == false)
            {

                speed = new Vector2(200,0);
            }

        }
        ////////////////////////////////////////////////////////////////////Methods Monogame
        public void Initialize()
        {

        }
        public void LoadContent(ContentManager content)
        {
            sTexture = content.Load<Texture2D>("MarioRight");
           
            AddAnimation(4);
        }
        public void  Update(GameTime gameTime)
        {
            base.Update(gameTime);
            Mouvement(gameTime);
            var kstate = Keyboard.GetState();
           
        }
        public void Draw(SpriteBatch spriBacth)
        {
            base.Draw(spriBacth);
        }
    }
}
