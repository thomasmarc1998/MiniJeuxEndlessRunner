﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Game2
{
    public class Ground
    {

        private List<Texture2D> groundList;
        private Texture2D ground;
        private Vector2 Position;
        public Texture2D texture
        {
            get { return ground; }
        }
        ////////////////////////////////////////////////////////////////////Constructeur
        public Ground(Vector2 position)
        {
            this.Position = position;
            groundList = new List<Texture2D>();
            for (int i = 0; i < 3200; i++)
            {
                groundList.Add(ground);
            }
        }
        ////////////////////////////////////////////////////////////////////Method Monogame
        public void LoadContent(ContentManager content)
        {
                ground =content.Load<Texture2D>("brick1");
                
        }
        public void Draw(SpriteBatch spriBacth)

        {
            int i = 0;
            foreach (Texture2D groundDessin in groundList)
            {
                i++;
                spriBacth.Draw(ground, new Vector2(-1280 - 16 + 16 * i,this.Position.Y+(this.Position.Y/2)-16), Color.White);
            }
           
        }

    }
}
