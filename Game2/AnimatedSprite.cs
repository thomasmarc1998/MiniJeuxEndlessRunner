﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game2
{
    abstract class AnimatedSprite
    {
        #region Fields
        /// The texture of the sprite
        protected Texture2D sTexture;
        /// The position of the SpriteObject
        private Vector2 sPosition;
        /// Array, that contains all rectangles used for the animation
        private Rectangle[] sRectangles;
        /// Number of frames in the animation
        private int frameIndex;
        /// Time that has passed since last frame change 
        private double timeElapsed;
        /// Time it takes to update a frame
        private double timeToUpdate;
        private Rectangle BoundingBox;
        /// Our time per frame is equal to 1 divided by frames per second(we are deciding FPS)
        private Vector2 Speed;
        private Vector2 _origin;
        ///////////////////////////////////////////////////////////////////setter,getter
        public Vector2 Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }
        public int FramesPerSecond
        {
            set { timeToUpdate = (1f / value); }
        }
        public Vector2 position
        {
            get { return sPosition; }
            set { sPosition =  value; }
        }
        public Vector2 speed
        {
            get { return Speed; }
            set { Speed = value; }
        }
        public float positionX
        {
            get { return sPosition.X; }
            set { sPosition.X = value; }
        }
        public float positionY
        {
            get { return sPosition.Y; }
            set { sPosition.Y = value; }
        }
         public Texture2D texture
         {
             get { return sTexture; }
             set { sTexture = value; }
         }
        public Rectangle boundingBox
        {
            get { return BoundingBox = new Rectangle((int)positionX, (int)positionY, sTexture.Width, sTexture.Height); }
            set { BoundingBox = value; }
        }

        #endregion
        ////////////////////////////////////////////////////////////////////Consctructeur
        protected AnimatedSprite(Vector2 Position)
        {
            this.sPosition = Position;
            this.Speed = new Vector2(200, 0);
        }
        //
        ////////////////////////////////////////////////////////////////////Methods de La class
        protected void AddAnimation(int frames)
        {   
            //Calculates the width of each frame
            int width = sTexture.Width / frames;

            //Creates an array of rectangles which will be used when playing an animation
            sRectangles = new Rectangle[frames];
            for (int i = 0; i < frames; i++)
            {
                sRectangles[i] = new Rectangle(i * width, 0, width, sTexture.Height);
            }
            //Fills up the array of rectangles
        }
        public virtual int OnCollide(AnimatedSprite sprite)
        {
            return 0;
        }
        ////////////////////////////////////////////////////////////////////Methods Monogame
        /// Determines when we have to change frames
        protected void Update(GameTime gameTime)
        {   
            //Adds time that has elapsed since our last draw
            timeElapsed += gameTime.ElapsedGameTime.TotalSeconds;
            //We need to change our image if our timeElapsed is greater than our timeToUpdate(calculated by our framerate)
                if (timeElapsed > timeToUpdate)
                {
                    //Resets the timer in a way, so that we keep our desired FPS
                    timeElapsed -= timeToUpdate;
                    //Adds one to our frameIndex
                    if (frameIndex < sRectangles.Length -1)
                    {
                        frameIndex++;
                    }
                    else //Restarts the animation
                    {
                        frameIndex = 0;
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.Space))
                    {
                        frameIndex = 0;
                    }
                }
        }
        /// Draws the sprite on the screen
        protected void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sTexture, sPosition, sRectangles[frameIndex], Color.White);
        }
    }
}
