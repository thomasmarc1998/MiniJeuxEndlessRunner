﻿#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;

#endregion

namespace Game2
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game2 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player player;
        Ground ground;
        Camera camera;
        Texture2D Background;
        Vector2 PositionBackground;
        CameraCursor cameraCursor;
        AnimatedSprite obstacle;
        Sprite ob;
        List<Sprite> obstacles;
        Random random = new Random();
        /////////////////////////////////////////////////////////////variable d'affichage
        SpriteFont text;
        float var = 1;
        int frameRate = 0;
        int frameCounter = 0;
        TimeSpan elapsedTime = TimeSpan.Zero;
        int vitesse = 0;
        int count = 0;
        bool ActiveTouche;

        //////////////////////////////////////////////////////////////Constructeur
        public Game2()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 960;  // set this value to the desired width of your window
            graphics.PreferredBackBufferHeight = 560;   // set this value to the desired height of your window

            graphics.IsFullScreen = false;
            obstacles = new List<Sprite>();
            graphics.ApplyChanges();
        }
        //////////////////////////////////////////////////////////////Fonction de Monogames
        protected override void Initialize()
        {
            player = new Player("toto", new Vector2(-400, 400));
            cameraCursor = new CameraCursor(player.speed, new Vector2(0, graphics.PreferredBackBufferHeight));
            PositionBackground = new Vector2(-graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2 + 16);
            ground = new Ground(new Vector2(0, graphics.PreferredBackBufferHeight));
            /////////////////////////////////////ajout des aléatoire d'obstacle dans la collection obstacles
            for (int i = 0; i < 1280; i++)
            {
                obstacles.Add(ob = new Sprite(new Vector2(0, graphics.PreferredBackBufferHeight + graphics.PreferredBackBufferHeight / 2 - 32 - 16), random.Next(1, 4)));
            }
            base.Initialize();
        }
        protected override void LoadContent()
        {
            ground.LoadContent(Content);
            player.LoadContent(Content);
            text = Content.Load<SpriteFont>("Fonts/Unbuntu32");
            Background = Content.Load<Texture2D>("Background");
            camera = new Camera(GraphicsDevice.Viewport);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            foreach (Sprite obLoad in obstacles)
            {
                obLoad.LoadContent(Content);
            }
        }
        protected override void UnloadContent()
        {
            Content.Unload();
        }
        protected override void Update(GameTime gameTime)
        {
            this.PositionBackground.X += cameraCursor.speed.X * (float)gameTime.ElapsedGameTime.TotalSeconds;
            var += cameraCursor.speed.X * (float)gameTime.ElapsedGameTime.TotalSeconds;
            player.Update(gameTime);
            cameraCursor.Update(gameTime);
            camera.Update(cameraCursor.Position);
            /////////////////////////////////////updates des obstacles
            foreach (Sprite obUpdate in obstacles)
            {
                obUpdate.Update(gameTime);
            }
            /////////////////////////////////////si le joueur touche un obstacle
            foreach (Sprite obExit in obstacles)
            {
                if (player.OnCollide(obExit) == 1)
                {

                   // this.Exit();
                   
                    Console.WriteLine("debug:");
                        player.position = new Vector2(-200, player.positionY);
                        player.speed = new Vector2(0, player.speed.Y);
                        cameraCursor.speed = new Vector2(200, 0);
                        cameraCursor.Position = new Vector2(player.positionX, graphics.PreferredBackBufferHeight);
                }
            }
            Trier();//tri La Colletion obstacle important!!! a placer avant le for
            /////////////////////////////////////replace la position des obstajcle si trop prés
            for (int i = 1; i < 639; i++)
            {
                  if (GetDistance(obstacles[i].boundingBox, obstacles[i + 1].boundingBox) >= 0 && GetDistance(obstacles[i].boundingBox, obstacles[i + 1].boundingBox) <= 128)
                  {
                      obstacles[i].positionX = random.Next((int)obstacles[i].positionX, (int)obstacles[i].positionX + 1000) * i;
                  }
            }
            /////////////////////////////////////Augmentation de la vitesse en fonction du score
            int score = (int)camera.X / 10;
            vitesse = (int)player.speed.X - 200;
            for (int i = 1; i <= 1000; i++)
            {
                if (score >= 100 * i)
                {
                    player.speed = new Vector2(200 + 10 * i, player.speed.Y + i / 100);

                    cameraCursor.speed = new Vector2(player.speed.X, 0);
                    vitesse = (int)player.speed.X - 200;
                    var = player.positionX-480;
                }
            }
            /////////////////////////////////////calcul FPS
            elapsedTime += gameTime.ElapsedGameTime;

            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }
            /////////////////////////////////////affiche info de débugage
            Console.WriteLine("largeur fenetre: " + GraphicsDevice.PresentationParameters.BackBufferWidth);
            Console.WriteLine("hauteur fenetre: " + GraphicsDevice.PresentationParameters.BackBufferHeight);
            Console.WriteLine("positionCurseur " + cameraCursor.Position);
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            frameCounter++;
            string fps = string.Format("fps: {0}", frameRate);
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.transform);
            // spriteBatch.Draw(Background, PositionBackground, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            player.Draw(spriteBatch);
            ground.Draw(spriteBatch);
            foreach (Sprite obDraw in obstacles)
            {
                obDraw.Draw(spriteBatch);
            }
            //////////////////////////affichage score
            int score = (int)camera.X / 10;
            float postext = cameraCursor.Position.X - 480;
            if (score>=1000)
            {
                spriteBatch.DrawString(text, "Score: " + score.ToString(), new Vector2(postext, 280), Color.Yellow);
            }
            else
            {
                spriteBatch.DrawString(text, "Score: " + score.ToString(), new Vector2(postext, 280), Color.White);
            }
            var kstate = Keyboard.GetState();
            //////////////////////////affichage fps
            if (kstate.IsKeyDown(Keys.F))
            {
                spriteBatch.DrawString(text, fps, new Vector2(postext, 320), Color.White);
            }
            //////////////////////////affichage vitesse
            spriteBatch.DrawString(text,"Vitesse: "+ vitesse.ToString(), new Vector2(postext, 300), Color.White);      
          
            spriteBatch.End();
            base.Draw(gameTime);
        }
        //////////////////////////////////////////////////////////////Fonction de la class
        private static double GetDistance(Rectangle point1, Rectangle point2)
        {
            //Get distance by using the pythagorean theorem
            double a = (double)(point2.X - point1.X);
            return a;
        }
        private void Trier()
        {
             obstacles = obstacles.OrderBy(x => x.positionX).ToList(); 
        }
    }
}
