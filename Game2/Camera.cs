﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
namespace Game2
{
    public class Camera
{
    private Matrix Transform;
    private float Zoom;
    private Vector2 centre;
    private Viewport viewport;

    ////////////////////////////////////////////////////////////////////Seter,Getter
    public float X
    {
        get { return centre.X; }
        set { centre.X = value; }
    }
    public float Y
    {
        get { return centre.Y; }
        set { centre.Y = value; }
    }
    public float zoom
    {
        get { return zoom; }
        set { zoom = value; if (zoom < 0.1f) zoom = 0.1f; }
    }
    public Matrix transform
    {
        get { return Transform; }
    }
    ////////////////////////////////////////////////////////////////////Constructeur
    public Camera(Viewport newViewport)
    {
        this.viewport = newViewport;
    }

    ////////////////////////////////////////////////////////////////////Methods
    public void Update(Vector2 position)
    {
        centre = new Vector2(position.X, position.Y);
        Transform = Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0)) * Matrix.CreateTranslation(new Vector3(viewport.Width / 2, viewport.Height / 2, 0));
    }


}

}

