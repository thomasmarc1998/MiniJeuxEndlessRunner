﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace Game2
{
    class CameraCursor
    {
        Vector2 CameraCursorPosition;
        Vector2 CameraCursorVitesse;
        ////////////////////////////////////////////////////////////////////setter,getter
        public Vector2 Position
        {
            get{return this.CameraCursorPosition;}
            set { this.CameraCursorPosition=value; }
        }
        public Vector2 speed
        {
            get { return this.CameraCursorVitesse; }
            set { CameraCursorVitesse = value; }
        }
        ////////////////////////////////////////////////////////////////////Constructeur
        public CameraCursor(Vector2 vitesse,Vector2 position)
        {
            this.CameraCursorPosition = position;
            this.CameraCursorVitesse = vitesse;

        }
        ////////////////////////////////////////////////////////////////////Methods class
        public void   Update(GameTime gameTime)
        {
            this.CameraCursorPosition.X += CameraCursorVitesse.X * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }
    }
}
