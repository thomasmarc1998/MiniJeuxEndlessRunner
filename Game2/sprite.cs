﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game2
{
     class Sprite : AnimatedSprite
    {
         private Vector2 m_positionSprite;
         private int choixTex;
         public int numTexture
         {
             get{return choixTex;}
         }
         public Sprite(Vector2 positionSprite,int choix) : base(positionSprite)
        {
            this.choixTex = choix;
        }
        public void Initialize()
        {

        }
        public void LoadContent(ContentManager content)
        {
            int choixTexture = this.choixTex;
            switch (choixTexture)
            {
                case 1:
                    sTexture = content.Load<Texture2D>("BlockPierre");
                    break;
                case 2:
                    sTexture = content.Load<Texture2D>("Tuyeau");
                    break;
                case 3:
                    sTexture = content.Load<Texture2D>("BlockMort");
                    break;
            }
           
            AddAnimation(1);
        }
        public override int OnCollide(AnimatedSprite sprite)
        {
            int touché = 0;
            #region
            if (sprite.boundingBox.X >= this.boundingBox.X + this.boundingBox.Width || sprite.boundingBox.X + sprite.boundingBox.Width <= this.boundingBox.X || sprite.boundingBox.Y >= this.boundingBox.Y + this.boundingBox.Height || sprite.boundingBox.Y + sprite.boundingBox.Height <= this.boundingBox.Y)
            {
                touché= 0;
            }
            else
            {
                touché= 1;
            }
            return touché;
        }
        public void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        public  void Draw(SpriteBatch spriteBacth)
        {
            spriteBacth.Draw(sTexture, this.position, null, Color.White);
        }
    }
  
}

#endregion